<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function phases() {
        return $this->hasMany(Phase::class);
    }

    public function event_teams() {
        //return $this->belongsToMany(Team::class, 'event_teams', 'event_id', 'team_id')
        //    ->withPivot('id as event_id')
        //    ->withPivot('points as event_points');

        return $this->hasMany(EventTeams::class);
    }
}
