<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'points', 'phase_id'];

    public function group_teams()
    {
        //return $this->belongsToMany(Team::class, 'group_teams', 'event_id', 'team_id')
        //    ->withPivot('id as group_teams_id')
        //    ->withPivot('points as group_points');

        return $this->hasMany(GroupTeams::class);
    }

    public function phase()
    {
        return $this->belongsTo(Phase::class);
    }
}
