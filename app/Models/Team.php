<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'points', 'medals', 'degree_id'];

    public function users() {
        return $this->hasMany(User::class);
    }

    public function degree() {
        return $this->belongsTo(Degree::class);
    }

    public function events() {
        return $this->belongsToMany(Event::class, 'event_teams', 'team_id', 'event_id')
            ->withPivot('points as event_points');
    }

    public function groups() {
        return $this->belongsToMany(Group::class, 'group_teams', 'team_id', 'event_id')
            ->withPivot('points as group_points');
    }
}
