<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchModel extends Model
{
    use HasFactory;

    protected $table = 'matchs';
    protected $fillable = ['name', 'first_team_id', 'second_team_id', 'winner_id', 'phase_id'];

    public function first_team()
    {
        return $this->belongsTo(Team::class, 'first_team_id');
    }

    public function second_team()
    {
        return $this->belongsTo(Team::class, 'second_team_id');
    }

    public function winner_team()
    {
        return $this->belongsTo(Team::class, 'winner_id');
    }

    public function phase()
    {
        return $this->belongsTo(Phase::class);
    }
}
