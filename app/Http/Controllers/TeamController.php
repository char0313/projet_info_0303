<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function update_score(Request $request) {
        $teamsData = $request->input('teams');

        // Iterate through the teams and save/update the data
        foreach ($teamsData as $teamId => $data) {
            // Assuming you have an Eloquent model for teams
            $team = Team::find($teamId);

            if ($team) {
                // Update the team data

                $team->medals = $data['medals'];
                $team->points = $data['points'];
                $team->save();
            }
        }

        return redirect()->back();
    }
}
