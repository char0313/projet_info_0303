<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function getTeams() {
        $teams = Team::select('teams.*', 'degrees.name as degree_name')
            ->join('degrees', 'teams.degree_id', '=', 'degrees.id')
            ->get();

        return response()->json($teams);
    }
}
