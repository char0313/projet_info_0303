<?php

namespace App\Http\Controllers;

use App\Models\MatchModel;
use App\Models\Phase;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function create_match(Request $request, $phase_id)
    {
        $request->validate([
            'team1_id' => 'required|exists:teams,id',
            'team2_id' => 'required|exists:teams,id',
            'name' => 'required|string|max:255',
            // Add any other validation rules as needed
        ]);

        // Find the phase by ID
        $phase = Phase::findOrFail($phase_id);

        $match = MatchModel::create([
            'first_team_id' =>  $request->input('team1_id'),
            'second_team_id' =>  $request->input('team2_id'),
            'name' => $request->input('name'),
            'phase_id' => $phase->id,
        ]);

        // You can redirect to a specific route or return a response as needed
        return redirect()->back()->with('success', 'Phase and associated data deleted successfully');
    }
    public function delete_match(Request $request, $match_id)
    {
        // Find the match by ID
        $match = MatchModel::findOrFail($match_id);

        // Delete the match
        $match->delete();

        return redirect()->back()->with('success', 'Match deleted successfully');
    }
    public function update_matches_results(Request $request)
    {
        // Validate the request if needed

        $data = $request->json()->all();

        foreach ($data as $match_id => $winner_id) {
            $match = MatchModel::findOrFail($match_id);

            if ($winner_id === 'null') {
                $winner_id = null;
            }

            // Update the winner_id
            $match->update(['winner_id' => $winner_id]);
        }

        return response()->json(['success' => true]);
    }
}
