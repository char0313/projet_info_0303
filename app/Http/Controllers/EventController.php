<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function create_event(Request $request) {
        $request->validate([
            'name' => 'required|string',
        ]);

        $event = Event::create(['name' => $request->input('name')]);
        $event->save();

        return redirect()->back()->with('success', 'Phase created successfully.');
    }

    public function delete_event(Request $request, $event_id) {
        $event = Event::findOrFail($event_id);

        $event->phases()->each(function ($phase) {

            // Delete associated matches
            $phase->matchs()->delete();

            // Delete associated groups and their team relationships
            $phase->groups()->each(function ($group) {
                foreach ($group->group_teams as $group_team) {
                    $group_team->delete();
                }
                $group->delete();
            });

            // Finally, delete the phase
            $phase->delete();

        });

        $event->delete();

        return redirect()->back()->with('success', 'Match deleted successfully');
    }
}
