<?php

namespace App\Http\Controllers;

use App\Models\Degree;
use App\Models\Event;
use App\Models\Team;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    public function index() {
        $events = Event::all();
        $teams = Team::all();
        $degrees = Degree::all();

        return view('results', compact(['events', 'teams', 'degrees']));

    }
}
