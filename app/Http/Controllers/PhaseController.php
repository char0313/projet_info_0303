<?php

namespace App\Http\Controllers;

use App\Models\EventTeams;
use App\Models\Phase;
use Illuminate\Http\Request;

class PhaseController extends Controller
{
    public function create_phase(Request $request, $event_id)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        $phase = Phase::create(['name' => $request->input('name'), 'event_id' => $event_id]);
        $phase->save();

        return redirect()->back()->with('success', 'Phase created successfully.');
    }

    public function delete_phase(Request $request, $phase_id) {
        // Find the phase by ID
        $phase = Phase::findOrFail($phase_id);

        // Delete associated matches
        $phase->matchs()->delete();

        // Delete associated groups and their team relationships
        $phase->groups()->each(function ($group) {
            foreach ($group->group_teams as $group_team) {
                $group_team->delete();
            }
            $group->delete();
        });

        // Finally, delete the phase
        $phase->delete();

        // Optionally, you can redirect back or return a response
        return redirect()->back()->with('success', 'Phase and associated data deleted successfully');
    }

    public function update_score_phase(Request $request) {
        $data = $request->input('ids');

        foreach ($data as $registrationId => $points) {
            // Update the points in the database for each registration
            $registration = EventTeams::findOrFail($registrationId); // Replace with your actual model
            $registration->update(['points' => $points]);
        }

        return redirect()->back()->with('success', 'Points updated successfully');

    }
}
