<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupTeams;
use App\Models\Phase;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function create_group(Request $request, $phase_id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            // Add any other validation rules as needed
        ]);

        $phase = Phase::findOrFail($phase_id);

        $group = Group::create([
            'name' => $request->input('name'),
            'phase_id' => $phase->id,
        ]);
        $group->save();

        // You can redirect to a specific route or return a response as needed
        return redirect()->back()->with('success', 'Phase and associated data deleted successfully');
    }

    public function delete_group(Request $request, $group_id)
    {
        $group = Group::findOrFail($group_id);

        foreach ($group->group_teams as $group_team) {
            $group_team->delete();
        }
        $group->delete();

        return redirect()->back()->with('success', 'Phase and associated data deleted successfully');
    }

    public function add_team_to_group(Request $request, $group_id) {
        // Validate the request
        $request->validate([
            'team_id' => 'required|exists:teams,id',
        ]);

        $group_teams = GroupTeams::create([
            'points' => 0,
            'team_id' => $request->input('team_id'),
            'group_id' => $group_id,
        ]);
        $group_teams->save();

        // You can add a success message if needed
        return redirect()->back()->with('success', 'Team added to group successfully');
    }

    public function remove_team_to_group(Request $request, $group_id, $team_id) {
        $group = Group::find($group_id);

        $groupTeam = GroupTeams::where('group_id', $group_id)->where('team_id', $team_id)->first();
        // Remove the team from the group
        $groupTeam->delete();

        // You can add a success message if needed
        return redirect()->back()->with('success', 'Team added to group successfully');
    }

    public function update_groups_points(Request $request)
    {
        // Validate the request if needed

        $data = $request->json()->all();

        // Iterate through the data and update the points
        foreach ($data as $teamId => $groupPoints) {
            foreach ($groupPoints as $groupId => $points) {
                // Find and update your model
                GroupTeams::where(['team_id' => $teamId, 'group_id' => $groupId])->update(['points' => $points]);
            }
        }

        // You can add a success message if needed
        return response()->json(['success' => true]);
    }
}
