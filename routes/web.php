<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use App\Http\Controllers\PhaseController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\ResultsController;
use App\Http\Controllers\TeamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

// results view
Route::get('/results', [ResultsController::class, 'index'])->name('results.index');

// Ajax requests
Route::post('/ajax/getTeams', [AjaxController::class, 'getTeams']);

// Edit Teams
Route::post('/teams/score', [TeamController::class, 'update_score'])->name('update.score.teams');

// Create and Delete Events
Route::post('/events/create', [EventController::class, 'create_event'])
    ->middleware('permission:create event')
    ->name('create.event');

Route::delete('/events/delete/{event_id}', [EventController::class, 'delete_event'])
    ->middleware('permission:delete event')
    ->name('delete.event');

// Edit Events and Phases
Route::group(['middleware' => ['permission:edit event']], function () {

    // Create and Delete Phases
    Route::post('/phases/{event_id}/create', [PhaseController::class, 'create_phase'])
        ->middleware('permission:create phase')
        ->name('create.phase');

    Route::delete('/phases/delete/{phase_id}', [PhaseController::class, 'delete_phase'])
        ->middleware('permission:delete phase')
        ->name('delete.phase');

    // Edit Phases
    Route::group(['middleware' => ['permission:edit phase']], function () {

        // Update Phase Score
        Route::post('/phases/update/score', [PhaseController::class, 'update_score_phase'])
            ->name('update.score.phase');

        // Create and Delete Groups
        Route::post('/phases/{phase_id}/create/group', [GroupController::class, 'create_group'])
            ->middleware('permission:create group')
            ->name('create.group');

        Route::delete('/phase/{group_id}/delete/group', [GroupController::class, 'delete_group'])
            ->middleware('permission:delete group')
            ->name('delete.group');

        // Edit Groups
        Route::group(['middleware' => ['permission:edit group']], function () {

            // Add, Remove, and Update Team in Group
            Route::post('/phases/{group_id}/add/team/to/group', [GroupController::class, 'add_team_to_group'])
                ->name('add.team.to.group');

            Route::delete('/phases/{group_id}/{team_id}/remove/team/to/group', [GroupController::class, 'remove_team_to_group'])
                ->name('remove.team.to.group');

            Route::post('/phases/update/groups/points', [GroupController::class, 'update_groups_points'])
                ->name('update.groups.points');
        });

        // Create, Delete, and Update Match
        Route::post('/phases/{phase_id}/create/match', [MatchController::class, 'create_match'])
            ->middleware('permission:create match')
            ->name('create.match');

        Route::delete('/phase/{match_id}/delete/match', [MatchController::class, 'delete_match'])
            ->middleware('permission:create match')
            ->name('delete.match');

        Route::post('/phases/update/matches/results', [MatchController::class, 'update_matches_results'])
            ->middleware('permission:edit match')
            ->name('update.matches.results');
    });
});


require __DIR__.'/auth.php';
