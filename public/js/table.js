var teams = null;
var sorted_by = 'medals';
var displayed_teams = null;
const row_per_pages = 6;
var page = 0;

fetch(ajaxUrl, {
    method: 'POST', // Change the method to POST
    headers: {
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': csrfToken, // Add CSRF token
    },
})
    .then(response => response.json())
    .then(data => {
        teams = Array.from(data);
        displayed_teams = teams.sort((a, b) => b[sorted_by] - a[sorted_by]);
    })
    .catch(error => console.error('Error fetching data:', error));

function toggleVisibility(event_name, phase_name) {
    const sections = document.getElementsByClassName(event_name);
    Array.from(sections).forEach(e => e.classList.contains('hidden') || e.classList.add('hidden'));
    document.getElementById(phase_name).classList.toggle('hidden');

    const selectors = document.getElementsByClassName('selector');
    const selectorsbin = document.getElementsByClassName('selectorbin');

    Array.from(selectors).forEach(element => {
        element.classList.toggle('bg-gray-100', element.classList.contains(phase_name));
    });

    Array.from(selectorsbin).forEach(element => {
        element.classList.toggle('hidden', !element.classList.contains(phase_name));
    });
}

function filter() {
    page = 0;
    document.getElementById('page').innerText = page + 1;
    const degrees = Array.from(document.querySelectorAll('input[type="checkbox"]:checked')).map(c => c.id);
    displayed_teams = teams.filter(teams => degrees.includes(teams['degree_name']));
    displayed_teams.sort((a, b) => b[sorted_by] - a[sorted_by]);

    update_table();
}

function sort() {
    page = 0;
    document.getElementById('page').innerText = page + 1;
    sorted_by = !document.getElementById('medailles_btn').classList.contains('hidden') ? 'medals' : 'points';
    document.getElementById('medailles_btn').classList.toggle('hidden');
    document.getElementById('points_btn').classList.toggle('hidden');

    displayed_teams.sort((a, b) => b[sorted_by] - a[sorted_by]);

    update_table();
}

function search_name() {
    page = 0;
    document.getElementById('page').innerText = page + 1;
    const input = document.getElementById('search');
    displayed_teams = teams.filter(team => team['name'].includes(input.value));

    displayed_teams.sort((a, b) => b[sorted_by] - a[sorted_by]);

    update_table();
}

function next_page() {
    if ((page + 1) * row_per_pages < displayed_teams.length) {
        page += 1;
        document.getElementById('page').innerText = page + 1;

        update_table();
    }
}

function previous_page() {
    if (page > 0) {
        page -= 1;
        document.getElementById('page').innerText = page + 1;

        update_table();
    }
}

function update_table() {
    var table = document.getElementById('teamsTable');
    var tbody = table.querySelector('tbody');
    tbody.innerHTML = '';

    for (var i = 0; i < row_per_pages; i += 1) {
        if (displayed_teams[i + page * row_per_pages] !== undefined) {
            var row = document.createElement('tr');
            row.classList.add('border-b');
            if (page === 0) row.classList.add(i === 0 ? 'bg-[#C9B037]/25' : (i === 1 ? 'bg-[#D7D7D7]/25' : (i === 2 ? 'bg-[#AD8A56]/25' : 'bg-transparent')));
            ['medals', 'points', 'name', 'degree_name'].forEach(a => {
                var b = document.createElement('td');
                b.classList.add('px-4', 'py-3', a);
                if (is_editor && (a === 'medals' || a === 'points')) {
                    var input = document.createElement('input');
                    input.classList.add('text-right', 'block', 'w-14', 'rounded-md', 'border-0', 'py-1.5', 'text-gray-900', 'ring-1', 'ring-inset', 'ring-gray-300', 'focus:ring-2', 'focus:ring-inset', 'focus:ring-indigo-600', 'sm:text-sm', 'sm:leading-6');
                    input.type = 'text';
                    input.name = `teams[${displayed_teams[i + page * row_per_pages]['id']}][${a}]`;
                    input.value = displayed_teams[i + page * row_per_pages][a];
                    b.appendChild(input);
                } else {
                    b.innerText = displayed_teams[i + page * row_per_pages][a];
                }
                row.appendChild(b);
            });
            tbody.appendChild(row);
        }
    }
}

function updatePoints(url1, url2, token) {
    // Collect all input values
    const inputs = document.querySelectorAll('.team-points');
    const data = {};

    inputs.forEach(input => {
        const nameParts = input.name.match(/teams\[(\d+)\]\[(\d+)\]/);

        if (nameParts) {
            const teamId = nameParts[1];
            const groupId = nameParts[2];
            const points = input.value;

            if (!data[teamId]) {
                data[teamId] = {};
            }

            data[teamId][groupId] = points;
        }
    });
    console.log(data);

    // Send data to the controller using fetch
    fetch(url1, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': token,
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .then(data => {
            // Handle the response if needed
            console.log(data);
            window.location.reload();
        })
        .catch(error => {
            // Handle errors
            console.error('Error:', error);
        });


    const match_data = {};
    const selects = document.querySelectorAll('select[id^="match"]');
    selects.forEach((select) => {
        const matchId = select.id.replace('match', '');
        match_data[matchId] = select.value;
    });
    console.log(match_data);

    fetch(url2, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': token,
        },
        body: JSON.stringify(match_data),
    })
        .then(response => response.json())
        .then(data => {
            // Handle the response if needed
            console.log(data);
            window.location.reload();
        })
        .catch(error => {
            // Handle errors
            console.error('Error:', error);
        });
}
