<?php

namespace Database\Seeders;

use App\Models\Degree;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DegreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Degree::create(['name' => 'SEN']);
        Degree::create(['name' => 'ODONTO']);
        Degree::create(['name' => 'ESI']);
        Degree::create(['name' => 'STAPS']);
    }
}
