<?php

namespace Database\Seeders;

use App\Models\Degree;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Models\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $degreeIds = Degree::all()->pluck('id')->toArray();

        for ($i = 0; $i < 100; $i++) {
            Team::create([
                'name' => $faker->unique()->word,
                'points' => $faker->numberBetween(0, 100),
                'medals' => $faker->numberBetween(0, 10),
                'degree_id' => $faker->randomElement($degreeIds),
            ]);
        }
    }
}
