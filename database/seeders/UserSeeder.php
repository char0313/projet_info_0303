<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => 'admin'
        ]);

        $staff = User::factory()->create([
            'name' => 'staff',
            'email' => 'staff@gmail.com',
            'password' => 'staff'
        ]);
        $staff->assignRole('staff');


        $faker = Faker::create();
        for ($i = 0; $i < 10; $i++) {
            User::factory()->create([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
            ]);
        }
    }
}
