<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\EventTeams;
use App\Models\Group;
use App\Models\GroupTeams;
use App\Models\MatchModel;
use App\Models\Phase;
use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $teams = Team::all();

        $handball = Event::create(['name' => 'handball']);
        foreach ($teams->random(4) as $team) {
            EventTeams::create(['points' => $faker->numberBetween(0, 100), 'team_id' => $team->id, 'event_id' => $handball->id]);
        }

        $handball_poule = Phase::create(['name' => 'Phase de poule', 'event_id' => $handball->id]);
        $poule_a = Group::create(['name' => 'Poule A', 'phase_id' => $handball_poule->id]);
        foreach ($handball->event_teams as $event_team) {
            GroupTeams::create(['points' => $faker->numberBetween(0, 100), 'team_id' => $event_team->team->id, 'group_id' => $poule_a->id]);
        }

        $poule_b = Group::create(['name' => 'Poule B', 'phase_id' => $handball_poule->id]);
        foreach ($handball->event_teams as $event_team) {
            GroupTeams::create(['points' => $faker->numberBetween(0, 100), 'team_id' => $event_team->team->id, 'group_id' => $poule_b->id]);
        }

        $poule_c = Group::create(['name' => 'Poule C', 'phase_id' => $handball_poule->id]);
        foreach ($handball->event_teams as $event_team) {
            GroupTeams::create(['points' => $faker->numberBetween(0, 100), 'team_id' => $event_team->team->id, 'group_id' => $poule_c->id]);
        }

        $handball_teams = $handball->event_teams;
        $handball_quart = Phase::create(['name' => 'Quart de finale', 'event_id' => $handball->id]);
        MatchModel::create(['name' => 'Quart A', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_quart->id]);
        MatchModel::create(['name' => 'Quart B', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_quart->id]);
        MatchModel::create(['name' => 'Quart C', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_quart->id]);
        MatchModel::create(['name' => 'Quart D', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_quart->id]);

        $handball_demi = Phase::create(['name' => 'Demi-finale', 'event_id' => $handball->id]);
        MatchModel::create(['name' => 'Demi Principale A', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_demi->id]);
        MatchModel::create(['name' => 'Demi Principale B', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_demi->id]);
        MatchModel::create(['name' => 'Demi Consolante A', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_demi->id]);
        MatchModel::create(['name' => 'Demi Consolante B', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_demi->id]);

        $handball_final = Phase::create(['name' => 'Finale', 'event_id' => $handball->id]);
        MatchModel::create(['name' => 'Finale Principale', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_final->id]);
        MatchModel::create(['name' => 'Finale Consolante', 'first_team_id' => $faker->randomElement($handball_teams)->team->id, 'second_team_id' => $faker->randomElement($handball_teams)->team->id, 'phase_id' => $handball_final->id]);
    }
}
