<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;


class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'create team']);
        Permission::create(['name' => 'edit team']);
        Permission::create(['name' => 'delete team']);

        Permission::create(['name' => 'create event']);
        Permission::create(['name' => 'edit event']);
        Permission::create(['name' => 'delete event']);

        Permission::create(['name' => 'create phase']);
        Permission::create(['name' => 'edit phase']);
        Permission::create(['name' => 'delete phase']);

        Permission::create(['name' => 'create match']);
        Permission::create(['name' => 'edit match']);
        Permission::create(['name' => 'delete match']);

        Permission::create(['name' => 'create group']);
        Permission::create(['name' => 'edit group']);
        Permission::create(['name' => 'delete group']);

        $staff = Role::create(['name' => 'staff']);
        $staff->givePermissionTo('create event');
        $staff->givePermissionTo('edit event');
        $staff->givePermissionTo('delete event');
        $staff->givePermissionTo('create phase');
        $staff->givePermissionTo('edit phase');
        $staff->givePermissionTo('delete phase');
        $staff->givePermissionTo('create match');
        $staff->givePermissionTo('edit match');
        $staff->givePermissionTo('delete match');
        $staff->givePermissionTo('create group');
        $staff->givePermissionTo('edit group');
        $staff->givePermissionTo('delete group');
        $staff->givePermissionTo('create team');
        $staff->givePermissionTo('edit team');
        $staff->givePermissionTo('delete team');
    }
}
